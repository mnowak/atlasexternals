ATLAS LCG Configuration For CMake
=================================

This package collects all the code for including LCG packages into the build
of ATLAS projects.

Projects needing an LCG release should set it up like:

    set( LCG_DIR ${CMAKE_SOURCE_DIR}.../AtlasLCG )
    set( LCG_VERSION_POSTFIX "" )
    find_package( LCG 87 REQUIRED )

The `LCG_VERSION_POSTFIX` variable can be used to pick up LCG releases like
`LCG_81b` and similar.

Once the LCG release was found, the project is expected to find the individual
LCG components using specific `find_package(...)` calls for those packages.
The `modules` subdirectory holds the "find modules" for all of the externals
that are supported for pickup from an LCG release.
